import products.ProductBase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Shop {
    // Каталог продуктов:
    private List<ProductBase> products;

    public Shop() {
        this.products = new ArrayList<>();
    }

    public void addProduct(ProductBase product) {
        this.products.add(product);
    }

    public List<ProductBase> getProducts() {
        return products;
    }
}
