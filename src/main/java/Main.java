import products.*;

import java.util.Scanner;

public class Main {
    Shop shop;

    Main() {
        shop = new Shop();
    }

    public static void main(String[] args) {
        (new Main()).start();
    }

    public void start() {
        System.out.println("Добро пожаловать в магазин мужских подарков!");
        System.out.println("Каталог продуктов:");
        fillShop();
        String input;
        Integer productNum;
        Scanner scanner = new Scanner(System.in);
        while (true) {
            printShop();
            System.out.println("\nВведите номер товара, чтобы больше узнать про него, либо 0 для выхода:");
            input = scanner.next();
            try {
                productNum = Integer.parseInt(input);
            } catch (Exception err) {
                System.out.println("Неправильный ввод!");
                continue;
            }
            if (productNum == 0) break;
            else if (productNum > shop.getProducts().size()) {
                System.out.println("Нет товара под этим номером.");
                continue;
            }
            System.out.println(shop.getProducts().get(productNum - 1).getDescription());
            System.out.println("Введите 0, чтобы вернуться к списку товаров.");
            scanner.next();
        }

    }

    private void fillShop() {
        // Гели для душа:
        shop.addProduct(new ShowerGel("Palmolive 'Нежный гель'", 150.0,
                "Малиновый йогурт", .5));
        shop.addProduct(new ShowerGel("Dolce Milk Chocolate", 100.0, "Шоколад", .2));
        shop.addProduct(new ShowerGel("Dove Men", 180.0,
                "Лимонная мята", .5));

        // Дезодоранты
        shop.addProduct(new Deodorant("Axe Shift", 180.0, "Спрей", .2));
        shop.addProduct(new Deodorant("Old Spice Swagger", 150.0, "Карандаш", .1));
        shop.addProduct(new Deodorant("Зеленое яблоко", 50.0, "Спрей", .3));

        // Шампуни
        shop.addProduct(new Shampoo("Head & Shoulders For Men", 283.0, "Жирные", .4));
        shop.addProduct(new Shampoo("Syoss Men", 298.0, "Сухие", .6));
        shop.addProduct(new Shampoo("Секреты бабушки Агафьи", 95.0, "Любые", .4));

        // Гели для бритья
        shop.addProduct(new ShavingGel("Arc Man", 254.0, "Все типы", .2));
        shop.addProduct(new ShavingGel("Gillette Fusion", 284.0, "Нежная кожа", .2));
        shop.addProduct(new ShavingGel("СТАРТ", 32.0, "Грубая кожа", .5));

        // Пиво
        shop.addProduct(new Beer("Guiness", 200.0, "Темное", .5));
        shop.addProduct(new Beer("Уральский мастер", 70.0, "Светлое", 2.0));
        shop.addProduct(new Beer("Балтика №9", 80.0, "Светлое", 1.75));
    }

    private void printShop() {
        Integer i = 1;
//        shop.getProducts().forEach((product) -> {
        for (ProductBase product : shop.getProducts()) {
            System.out.printf("%d: %s %s, %.2f руб.\n", i, product.getCategoryName(), product.getTitle(), product.getPrice());
            i++;
        }
    }
}
