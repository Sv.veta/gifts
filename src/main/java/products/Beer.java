package products;

import java.util.Calendar;

public class Beer extends ProductBase {
    // Цвет:
    private String color;
    // Объем:
    private Double volume;

    public Beer(String title, Double price, String color, Double volume) {
        super(title, price);
        this.color = color;
        this.volume = volume;
    }

    @Override
    public String getCategoryName() {
        return "Пиво";
    }

    @Override
    public Double getWeekDiscount() {
        Integer weekDay = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        if (weekDay.equals(2)) return .1;
        else return .0;
    }

    @Override
    public String getExtraDescription() {
        return String.format("Цвет: %s\n" +
                "Объем: %.2f л\n", color, volume);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

}
