package products;

import java.util.Calendar;

public class Deodorant extends ProductBase {
    // Тип:
    private String type;
    // Объем:
    private Double volume;

    public Deodorant(String title, Double price, String type, Double volume) {
        super(title, price);
        this.type = type;
        this.volume = volume;
    }

    @Override
    public String getCategoryName() {
        return "Дезодорант";
    }

    @Override
    public Double getWeekDiscount() {
        Integer weekDay = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        if (weekDay.equals(3)) return .1;
        else return .0;
    }

    @Override
    public String getExtraDescription() {
        return String.format("Тип: %s\n" +
                "Объем: %.2f л\n", type, volume);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }
}
