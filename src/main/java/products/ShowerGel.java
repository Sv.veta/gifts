package products;

import java.util.Calendar;

public class ShowerGel extends ProductBase {
    // Вкус:
    private String flavour;
    // Объем:
    private Double volume;

    public ShowerGel(String title, Double price, String flavour, Double volume) {
        super(title, price);
        this.flavour = flavour;
        this.volume = volume;
    }

    @Override
    public String getCategoryName() {
        return "Гель для душа";
    }

    @Override
    public Double getWeekDiscount() {
        Integer weekDay = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        if (weekDay.equals(1)) return .1;
        else return .0;
    }

    @Override
    public String getExtraDescription() {
        return String.format("Вкус: %s\n" +
                "Объем: %.2f л\n", flavour, volume);
    }

    public String getFlavour() {
        return flavour;
    }

    public void setFlavour(String flavour) {
        this.flavour = flavour;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }
}
