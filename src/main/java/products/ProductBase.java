package products;

import java.util.Calendar;

public abstract class ProductBase {
    // Наименование:
    private String title;
    // Цена:
    private Double price;

    public ProductBase(String title, Double price) {
        this.title = title;
        this.price = price;
    }

    public Double getMonthDiscount() {
        String[][] rules = {
                {"1", "О"}, {"2", "Е"}, {"3", "А"}, {"4", "И"}, {"5", "Н"}, {"6", "Т"}, {"7", "С"},
                {"8", "Р"}, {"9", "В"}, {"10", "Л"}, {"11", "К"}, {"12", "М"}, {"13", "Д"}, {"14", "П"},
                {"15", "У"}, {"16", "Я"}, {"17", "Г"}, {"18", "C"}, {"19", "F"}, {"20", "E"}, {"21", "A"}, {"22", "T"},
                {"23", "O"}, {"24", "N"}, {"25", "R"}, {"26", "I"}, {"27", "S"}, {"28", "H"}, {"29", "D"}, {"30", "L"}
        };
        Integer monthDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        for (String[] weekAndNumber : rules) {
            if (monthDay.equals(Integer.parseInt(weekAndNumber[0])) &&
                    getTitle().toUpperCase().startsWith(weekAndNumber[1])) {
                return .15;
            }
        }
        return .0;
    }

    // Получить процент скидки:
    public Double getDiscount() {
        return 1 - getMonthDiscount() - getWeekDiscount();
    }

    // Абстрактные методы необходимо переопределить в потомках:
    // -----
    // Получить наименовании категории товара:
    public abstract String getCategoryName();

    // День недели, в который на этот товар скидка
    public abstract Double getWeekDiscount();

    // Получить дополнительное описание
    public abstract String getExtraDescription();

    public String getDescription() {
        String descr = String.format("Продукт %s\n" +
                "Категория: %s\n" +
                "Цена: %.2f рублей\n" +
                "Цена со скидкой: %.2f рублей",
                getTitle(), getCategoryName(), getPrice(), getPriceWithDiscount());
        return descr + "\n" + this.getExtraDescription();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getPrice() {
        return price;
    }

    public Double getPriceWithDiscount() {
        return price * getDiscount();
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
