package products;

import java.util.Calendar;

public class Shampoo extends ProductBase {
    // Тип волос:
    private String hairType;
    // Объем:
    private Double volume;

    public Shampoo(String title, Double price, String hairType, Double volume) {
        super(title, price);
        this.hairType = hairType;
        this.volume = volume;
    }

    @Override
    public String getCategoryName() {
        return "Шампунь";
    }

    @Override
    public Double getWeekDiscount() {
        Integer weekDay = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        if (weekDay.equals(4)) return .1;
        else return .0;
    }

    @Override
    public String getExtraDescription() {
        return String.format("Для типов волос: %s\n" +
                "Объем: %.2f л\n", hairType, volume);
    }

    public String getHairType() {
        return hairType;
    }

    public void setHairType(String hairType) {
        this.hairType = hairType;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }
}
