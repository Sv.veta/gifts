package products;

import java.util.Calendar;

public class ShavingGel extends ProductBase {
    // Вкус:
    private String skinType;
    // Объем:
    private Double volume;

    public ShavingGel(String title, Double price, String skinType, Double volume) {
        super(title, price);
        this.skinType = skinType;
        this.volume = volume;
    }

    @Override
    public String getCategoryName() {
        return "Гель для бритья";
    }

    @Override
    public Double getWeekDiscount() {
        Integer weekDay = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        if (weekDay.equals(5)) return .1;
        else return .0;
    }

    @Override
    public String getExtraDescription() {
        return String.format("Для типов кожи: %s\n" +
                "Объем: %.2f л\n", skinType, volume);
    }

    public String getSkinType() {
        return skinType;
    }

    public void setSkinType(String skinType) {
        this.skinType = skinType;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }
}
